package com.test1.demo.service;

import com.test1.demo.domain.Person;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    public List<Person> getPersons() {
        List<Person> personList = new ArrayList<>();

        personList.add(new Person("Jan", "jan@onet.pl"));
        personList.add(new Person("Mike", "mike@onet.pl"));
        personList.add(new Person("Tom", "tom@onet.pl"));
        personList.add(new Person("Don", "don@onet.pl"));

        return personList;
    }
}
