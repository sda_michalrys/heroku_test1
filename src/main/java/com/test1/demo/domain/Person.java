package com.test1.demo.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Setter
@Getter
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    
    @Column
    private String firstName;
    
    @Column
    private String email;

    public Person() {
    }

    public Person(String firstName, String email) {
        this.firstName = firstName;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
