package com.test1.demo.web;

import com.test1.demo.domain.Person;
import com.test1.demo.persistance.PersonRepository;
import com.test1.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonRepository personRepository;

    @PostConstruct
    public void initializer() {
        personRepository.saveAll(personService.getPersons());
    }

    @GetMapping(value = "/")
    public String indexPage(Model model) {
        Person admin = personRepository.findByEmail("mike@onet.pl");

        String adminName = admin.getFirstName();
        model.addAttribute("admin", adminName);

        return "index";
    }
}