package com.test1.demo.persistance;

import com.test1.demo.domain.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
    public Person findByEmail(String email);
}
